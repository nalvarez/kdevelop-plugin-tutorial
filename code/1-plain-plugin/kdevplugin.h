#ifndef KDEVPLUGIN_H
#define KDEVPLUGIN_H

#include <interfaces/iplugin.h>

class KDevPlugin : public KDevelop::IPlugin
{
    Q_OBJECT

public:
    //every KF5 plugin should have a constructor with this signature
    KDevPlugin(QObject* parent, const QVariantList& args);
};

#endif
