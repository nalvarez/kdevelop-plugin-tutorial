#include "kdevplugin.h"

#include <QDebug>

{%- macro kplugin_macro() -%}
#include <KPluginFactory>
K_PLUGIN_FACTORY_WITH_JSON(KDevMyPluginFactory, "kdevplugin.json", registerPlugin<KDevPlugin>(); )
{%- endmacro %}
{%- if has_plugin_factory %}

{{ kplugin_macro() }}
{%- endif %}

KDevPlugin::KDevPlugin(QObject *parent, const QVariantList& args) :
    KDevelop::IPlugin(QStringLiteral("kdevplugin"), parent)
{
    qDebug() << "My plugin is loaded!";
}

{% if has_plugin_factory %}
#include "kdevplugin.moc"
{% endif %}
