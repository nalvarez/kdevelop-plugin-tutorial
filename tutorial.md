% Writing a KDevelop plugin

## Basics

I will start by showing how to make the simplest possible KDevelop plugin.

All KDevelop plugins need a QObject class deriving from `KDevelop::IPlugin`. Let's declare it:

{{ snippet('code/1-plain-plugin/kdevplugin.h', 'cpp') }}

The constructor of our plugin must pass the plugin name and the `parent` QObject
to `IPlugin`'s constructor:

{{ snippet('code/1-plain-plugin/kdevplugin.cpp', 'cpp') }}

And finally, we need a CMake build system to compile it:

{{ snippet('code/1-plain-plugin/CMakeLists.txt', 'cmake', comments=True) }}

{{ try_compile() }}

At this point you should try actually compiling the code,
to ensure your build environment is set up correctly.
Remember to pass a custom `-DCMAKE_INSTALL_PREFIX=`;
don't leave it on the default `/usr/local`.

So far this is pretty simple C++/Qt code, and should be self-explanatory.
Now we need to add a few more things before this can be loaded as a plugin.

All Qt5 plugins contain an embedded JSON file, with metadata that is read
before the plugin is actually loaded.
KF5 defines what information that JSON should actually contain.

Here is a simple JSON file with the minimum information required:

{{ snippet('code/1-plain-plugin/kdevplugin.json', 'json') }}

Within `KPlugin`, the `Id` key should give a unique identifier for the plugin.
The `Name` key has a short human-readable name;
it will be shown in the KDevelop plugin settings along with `Description`.

The `ServiceTypes` key *must* contain `KDevelop/Plugin`.

{# this needs better explanation #}
The `X-KDevelop-Category` key defines when the plugin is loaded.
We will use `"Global"` for now,
which means the plugin is always loaded when KDevelop starts.

`"X-KDevelop-Mode"` says whether the plugin requires a GUI.
This key is required.
Setting it to `"NoGUI"` will allow the plugin to load
when KDevPlatform components are used outside the graphical KDevelop IDE
(which includes GUI-less unit tests!).
In most cases you will want to set it to `"GUI"`.

KF5-based plugins also need a "plugin factory".
The following macro takes care of defining the factory,
and embedding the metadata JSON file into the library:

{% import 'code/1-plain-plugin/kdevplugin.cpp' as cpp %}
{{ cpp.kplugin_macro() | mini_snippet('cpp', startLine=5) }}

Note that the `K_PLUGIN_FACTORY_WITH_JSON` macro defines a QObject class,
so we need to ensure `moc` runs for our `.cpp`, by including `kdevplugin.moc`
at the end of the file. This is particularly important because `moc` is what
causes the JSON file to be embedded into the plugin.

Our `kdevplugin.cpp` now looks like this:

{{ snippet('code/1-plain-plugin/kdevplugin.cpp', 'cpp', has_plugin_factory=True) }}

{{ try_compile() }}

Our build system is missing a few pieces,
which aren't trivial to write by ourselves.
We would need an installation rule to install the plugin
into the correct directory for KDevelop to find it.
In addition, we would need to add the JSON file as a dependency,
so that `moc` and the compiler are run again if the json changes.
This needs a relatively uncommon CMake command.

Fortunately, KDevPlatform provides a macro that handles all that for us.
All we have to do is replace the `add_library` with a call to `kdevplatform_plugin`,
passing it the name of the json file too:

{{ snippet('code/1-plain-plugin/CMakeLists.txt', 'cmake', kdevplatform_plugin=True) }}

{{ try_compile() }}

Compile it again, and install it (`make install`).

{# this needs to be explained better; the location depends on the architecture, and on whether
the system uses multiarch at all #}
Now, add `$my_install_prefix/lib/x86_64-linux-gnu/plugins` to the `QT_PLUGIN_PATH` environment variable,
and run `kdevelop` from a console. If everything went well, you should see "`My plugin is loaded!`"
printed in the console.
