#!/usr/bin/env python3

import sys
import os
import shutil
import subprocess
from jinja2 import Environment, FileSystemLoader, Markup

env = Environment(loader=FileSystemLoader('.'))

source_files = {}

compile_count=0

# function
def snippet(path, language, **kwargs):
    template = env.get_template(path)
    code = template.render(**kwargs).strip()
    source_files[path] = code

    ret = '~~~ {.%s .numberLines}\n' % language
    ret += code
    ret += '\n~~~'
    return ret

# filter
def mini_snippet(code, language, numberLines=False, startLine=None):
    if startLine != None:
        numberLines = True

    ret = '~~~ {.%s' % language
    if numberLines:
        ret += ' .numberLines'
    if startLine != None:
        ret += ' startFrom=%d' % startLine
    ret += '}\n'
    ret += code
    ret += '\n~~~'
    return ret

def dont_render(caller):
    caller()
    return ''

def try_compile():
    global compile_count
    compile_count += 1
    print("Trying to compile (%d)..." % compile_count, file=sys.stderr)
    TEMP_DIR='try_compile.tmp'
    try:
        shutil.rmtree(TEMP_DIR)
    except FileNotFoundError:
        pass
    os.mkdir(TEMP_DIR)

    for path, content in source_files.items():
        path = os.path.join(TEMP_DIR, os.path.basename(path))
        with open(path, 'w') as f:
            f.write(content)

    BUILD_DIR = os.path.join(TEMP_DIR, 'build')
    os.mkdir(BUILD_DIR)
    try:
        subprocess.check_output(['cmake', '-G', 'Ninja'] + sys.argv[3:] + ['..'], cwd=BUILD_DIR, stderr=subprocess.STDOUT)
        subprocess.check_output(['ninja'], cwd=BUILD_DIR)
    except subprocess.CalledProcessError as e:
        ret = Markup('<div style="border: solid red 1px; padding: 0 0.5em"><p style="color: red; font-weight: bold">There is a compiler error above!</p>')
        ret += Markup('<pre>') + e.output.decode('utf-8') + Markup('</pre></div>')
        return ret
    return ''

env.filters['mini_snippet'] = mini_snippet
env.globals['snippet'] = snippet
env.globals['dont_render'] = dont_render
env.globals['try_compile'] = try_compile

template = env.get_template(sys.argv[1])

print("rendering", file=sys.stderr)
with open(sys.argv[2], 'w') as outfile:
    for chunk in template.generate():
        outfile.write(chunk)
print("done!", file=sys.stderr)
